import React, { Fragment } from 'react';

import MuiToolbar from '@material-ui/core/Toolbar';

import TopBar from './components/TopBar';
import VideoPlayer from './components/VideoPlayer';
import { SubtitlessProvider } from './context/subtitles.context';

function App() {
	return (
		<Fragment>
			<TopBar />
			<MuiToolbar />
			<SubtitlessProvider>
				<VideoPlayer />
			</SubtitlessProvider>
		</Fragment>
	);
}

export default App;
