import React, { forwardRef } from 'react';

import Grid from '@material-ui/core/Grid';

import { useStyles } from './index.styles';

import Header from './Header';
import Footer from './Footer';

const local = {
	videoControls: {
		title: 'Movie'
	}
};

const VideoControls = forwardRef(
	(
		{
			onPlayPause,
			played,
			isPlaying,
			volumeState,
			onVolumeChange,
			onMute,
			onRewind,
			onFastForward,
			elapsedTime,
			totalDuration,
			onSeek,
			onSeekMouseUp,
			onSeekMouseDown
		},
		ref
	) => {
		const classes = useStyles();
		const { title } = local.videoControls;

		return (
			<div ref={ref} className={classes.overlay}>
				<Grid container direction="column" justify="space-between" style={{ flexGrow: 1 }}>
					<Header title={title} />
					<Footer
						{...{
							onPlayPause,
							played,
							isPlaying,
							volumeState,
							onVolumeChange,
							onMute,
							onRewind,
							onFastForward,
							elapsedTime,
							totalDuration,
							onSeek,
							onSeekMouseUp,
							onSeekMouseDown
						}}
					/>
				</Grid>
			</div>
		);
	}
);

export default VideoControls;
