import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles({
	overlay: {
		visibility: 'hidden',
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		bottom: 0,
		background: 'rgba(0,0,0,0.6)',
		display: 'flex',
		flexDirection: 'column',
		justifyContent: 'space-between'
	}
});
