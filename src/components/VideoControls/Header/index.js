import React from 'react';

import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import SubtitleDialog from '../../SubtitleDialog';

const Header = ({ title }) => {
	return (
		<Grid container direction="row" alignItems="center" justify="space-between" style={{ padding: 16 }}>
			<Grid item>
				<Typography variant="h5" style={{ color: '#fff' }}>
					{title}
				</Typography>
			</Grid>
			<Grid item>
				<SubtitleDialog />
			</Grid>
		</Grid>
	);
};

export default Header;
