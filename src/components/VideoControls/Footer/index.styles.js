import { makeStyles, withStyles } from '@material-ui/core/styles';

import Slider from '@material-ui/core/Slider';

export const useStyles = makeStyles((theme) => ({
	bottomIcons: {
		color: '#999',
		'&:hover': {
			color: '#fff'
		}
	},

	volumeSlider: {
		width: 100,
		color: theme.palette.common.white
	},
	PrettoSlider: {
		color: theme.palette.secondary.main
	}
}));

export const PrettoSlider = withStyles({
	root: {
		height: 8
	},
	thumb: {
		height: 24,
		width: 24,
		backgroundColor: '#fff',
		border: '2px solid currentColor',
		marginTop: -8,
		marginLeft: -12,
		'&:focus, &:hover, &$active': {
			boxShadow: 'inherit'
		}
	},
	active: {},
	valueLabel: {
		left: 'calc(-50% + 4px)'
	},
	track: {
		height: 8,
		borderRadius: 4
	},
	rail: {
		height: 8,
		borderRadius: 4
	}
})(Slider);
