import React from 'react';

import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Tooltip from '@material-ui/core/Tooltip';
import VolumeUpIcon from '@material-ui/icons/VolumeUp';
import VolumeDownIcon from '@material-ui/icons/VolumeDown';
import VolumeOffIcon from '@material-ui/icons/VolumeOff';
import Slider from '@material-ui/core/Slider';
import PlayArrowIcon from '@material-ui/icons/PlayArrow';
import PauseIcon from '@material-ui/icons/Pause';
import Replay10Icon from '@material-ui/icons/Replay10';
import Forward10Icon from '@material-ui/icons/Forward10';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import { useStyles, PrettoSlider } from './index.styles';

const Footer = ({
	onPlayPause,
	played,
	isPlaying,
	volumeState: { volume, muted },
	onVolumeChange,
	onMute,
	onRewind,
	onFastForward,
	onVolumeSeekDown,
	elapsedTime,
	totalDuration,
	onSeek,
	onSeekMouseUp,
	onSeekMouseDown
}) => {
	const classes = useStyles();

	const ValueLabelComponent = (props) => {
		const { children, open, value } = props;

		return (
			<Tooltip className={classes.PrettoSlider} open={open} enterTouchDelay={0} placement="top" title={value}>
				{children}
			</Tooltip>
		);
	};
	return (
		<Grid container direction="row" justify="space-between" alignItems="center" style={{ padding: 16 }}>
			<Grid item xs={12}>
				<PrettoSlider
					min={0}
					max={100}
					ValueLabelComponent={(props) => <ValueLabelComponent {...props} value={elapsedTime} />}
					aria-label="custom thumb label"
					value={played * 100}
					onChange={onSeek}
					onMouseDown={onSeekMouseDown}
					onChangeCommitted={onSeekMouseUp}
					className={classes.PrettoSlider}
				/>
			</Grid>

			<Grid item>
				<Grid container alignItems="center">
					<IconButton onClick={onRewind} className={classes.bottomIcons} aria-label="rewind">
						<Replay10Icon fontSize="large" />
					</IconButton>
					<IconButton onClick={onPlayPause} className={classes.bottomIcons}>
						{isPlaying ? <PauseIcon fontSize="large" /> : <PlayArrowIcon fontSize="large" />}
					</IconButton>
					<IconButton onClick={onFastForward} className={classes.bottomIcons} aria-label="forward">
						<Forward10Icon fontSize="large" />
					</IconButton>
					<IconButton className={`${classes.bottomIcons} ${classes.volumeButton}`} onClick={onMute}>
						{muted ? (
							<VolumeOffIcon fontSize="large" />
						) : volume > 0.5 ? (
							<VolumeUpIcon fontSize="large" />
						) : (
							<VolumeDownIcon fontSize="large" />
						)}
					</IconButton>

					<Slider
						min={0}
						max={100}
						aria-labelledby="input-slider"
						className={classes.volumeSlider}
						value={muted ? 0 : volume * 100}
						onChange={onVolumeChange}
						onMouseDown={onSeekMouseDown}
						onChangeCommitted={onVolumeSeekDown}
					/>
					<Button variant="text">
						<Typography variant="body1" style={{ color: '#fff', marginLeft: 16 }}>
							{elapsedTime}/{totalDuration}
						</Typography>
					</Button>
				</Grid>
			</Grid>
		</Grid>
	);
};

export default Footer;
