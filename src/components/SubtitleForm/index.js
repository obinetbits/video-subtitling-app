import React, { Fragment, useState, useContext } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import AddIcon from '@material-ui/icons/Add';
import SaveIcon from '@material-ui/icons/Save';

import { DispatchContext } from '../../context/subtitles.context';

const useStyles = makeStyles({
	time: {
		maxWidth: '110px'
	},
	subtitle: {
		width: '100%'
	},
	form: {
		width: '100%'
	},
	iconBtn: {
		maxWidth: '60px'
	}
});

const AddSubtitleForm = ({ editForm, subtitle, toggle }) => {
	const classes = useStyles();
	const dispatch = useContext(DispatchContext);
	const [ startTime, setStartTime ] = useState(subtitle.startTime || '');
	const [ endTime, setEndTime ] = useState(subtitle.endTime || '');
	const [ subtitleText, setSubtitleText ] = useState(subtitle.text || '');

	const handleStartTimeInput = (e) => {
		setStartTime(e.target.value);
	};
	const handleEndTimeInput = (e) => {
		setEndTime(e.target.value);
	};
	const handleSubtitleTextInput = (e) => {
		setSubtitleText(e.target.value);
	};
	const onSubmit = (e) => {
		e.preventDefault();
		const payload = { ...{ startTime, endTime, text: subtitleText, ...(editForm && { id: subtitle.id }) } };
		const type = editForm ? 'EDIT' : 'ADD';
		dispatch({ type, payload });

		if (!editForm) {
			setStartTime('');
			setEndTime('');
			setSubtitleText('');
		}
		if (editForm) {
			toggle();
		}
	};

	return (
		<Fragment>
			<form noValidate autoComplete="off" className={classes.form}>
				<Grid container spacing={1}>
					<Grid container item xs={12} spacing={3}>
						<Grid item xs={4} className={classes.time}>
							<TextField
								value={startTime}
								onChange={handleStartTimeInput}
								id="standard-number"
								type="number"
								label="Start time"
							/>
						</Grid>
						<Grid item xs={4} className={classes.time}>
							<TextField
								value={endTime}
								onChange={handleEndTimeInput}
								id="standard-number"
								type="number"
								label="End time"
							/>
						</Grid>
						<Grid item xs={4} className={classes.subtitle}>
							<TextField
								fullWidth
								value={subtitleText}
								onChange={handleSubtitleTextInput}
								id="standard-basic"
								label="Subtitle text"
							/>
						</Grid>
						<Grid item xs={4} className={classes.iconBtn}>
							{editForm ? (
								<IconButton onClick={onSubmit} aria-label="Save">
									<SaveIcon />
								</IconButton>
							) : (
								<IconButton onClick={onSubmit} aria-label="Add">
									<AddIcon />
								</IconButton>
							)}
						</Grid>
					</Grid>
				</Grid>
			</form>
		</Fragment>
	);
};

export default AddSubtitleForm;
