import React, { Fragment, useContext } from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import Paper from '@material-ui/core/Paper';
import SubtitlesIcon from '@material-ui/icons/Subtitles';
import { makeStyles } from '@material-ui/core/styles';
import Divider from '@material-ui/core/Divider';

import SubtitleForm from '../SubtitleForm';
import SubtitleItem from '../SubtitleItem';
import { SubtitlesContext } from '../../context/subtitles.context';

const useStyles = makeStyles((theme) => ({
	inputForm: {
		padding: '16px 24px'
	},
	subtitleBtn: {
		backgroundColor: 'transparent',
		color: 'rgb(255, 255, 255)',
		'&:hover': {
			backgroundColor: theme.palette.secondary.main,
			color: 'rgb(255, 255, 255)'
		}
	}
}));

const SubtitleDialog = () => {
	const classes = useStyles();
	const subtitles = useContext(SubtitlesContext);
	const [ open, setOpen ] = React.useState(false);

	const handleClickOpen = () => {
		setOpen(true);
	};

	const handleClose = () => {
		setOpen(false);
	};

	return (
		<Fragment>
			<Button
				onClick={handleClickOpen}
				variant="contained"
				className={classes.subtitleBtn}
				startIcon={<SubtitlesIcon />}
			>
				{'Add Subtitle'}
			</Button>
			<Dialog fullScreen open={open} onClose={handleClose} aria-labelledby="form-dialog-title">
				<DialogTitle id="form-dialog-title">Subtitle</DialogTitle>
				<Fragment>
					<Paper className={classes.inputForm}>
						<SubtitleForm editForm={false} subtitle={{}} />
					</Paper>
					<Paper>
						{subtitles.map((subtitle, i) => (
							<React.Fragment key={i}>
								<SubtitleItem {...subtitle} key={subtitle.id} />
								{i < subtitles.length - 1 && <Divider />}
							</React.Fragment>
						))}
					</Paper>
				</Fragment>
				<DialogActions>
					<Button onClick={handleClose} color="primary">
						{'Close'}
					</Button>
				</DialogActions>
			</Dialog>
		</Fragment>
	);
};

export default SubtitleDialog;
