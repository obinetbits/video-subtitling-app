import React, { Fragment, useContext } from 'react';

import { makeStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';

import SubtitleForm from '../SubtitleForm';
import useToggleState from '../../hooks/useToggleState';
import { DispatchContext } from '../../context/subtitles.context';

const useStyles = makeStyles({
	root: {
		flexGrow: 1
	},
	submit: {
		marginTop: '20px',
		minWidth: 200,
		alignSelf: 'center'
	},
	time: {
		maxWidth: '110px'
	},
	subtitle: {
		maxWidth: '90px'
	},
	inputForm: {
		padding: '16px 24px'
	},
	btn: {
		maxWidth: '60px'
	}
});

const SubtitleItem = ({ id, startTime, endTime, text }) => {
	const classes = useStyles();
	const [ isEditing, toggle ] = useToggleState(false);
	const dispatch = useContext(DispatchContext);

	return (
		<ListItem style={{ height: '64px' }}>
			{isEditing ? (
				<SubtitleForm id={id} editForm={true} toggle={toggle} subtitle={{ id, startTime, endTime, text }} />
			) : (
				<Fragment>
					<ListItemText className={classes.time}>{startTime}</ListItemText>
					<ListItemText className={classes.time}>{endTime}</ListItemText>
					<ListItemText>{text}</ListItemText>
					<ListItemSecondaryAction>
						<IconButton aria-label="Delete" onClick={() => dispatch({ type: 'REMOVE', payload: { id } })}>
							<DeleteIcon />
						</IconButton>
						<IconButton onClick={toggle} aria-label="Edit">
							<EditIcon />
						</IconButton>
					</ListItemSecondaryAction>
				</Fragment>
			)}
		</ListItem>
	);
};

export default SubtitleItem;
