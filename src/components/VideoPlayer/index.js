import React, { useRef, useState, useContext } from 'react';

import ReactPlayer from 'react-player';

import Container from '@material-ui/core/Container';

import { useStyles } from './index.styles';
import VideoControls from '../VideoControls';
import { SubtitlesContext } from '../../context/subtitles.context';

const format = (seconds) => {
	if (isNaN(seconds)) {
		return `00:00`;
	}
	const date = new Date(seconds * 1000);
	const hh = date.getUTCHours();
	const mm = date.getUTCMinutes();
	const ss = date.getUTCSeconds().toString().padStart(2, '0');
	if (hh) {
		return `${hh}:${mm.toString().padStart(2, '0')}:${ss}`;
	}
	return `${mm}:${ss}`;
};

const VideoPlayer = () => {
	const classes = useStyles();
	const subtitles = useContext(SubtitlesContext);
	const reactPlayerRef = useRef(null);
	const [ isPlaying, setPlaying ] = useState(true);
	const [ played, setPlayed ] = useState(0);
	const [ volumeState, setVolumeState ] = useState({ volume: 1, muted: false });
	const count = useRef(0);
	const videoControlsRef = useRef(null);
	const innerContainerRef = useRef(null);
	const videoRef = useRef(null);
	const videoTrackRef = useRef(null);
	const [ seeking, setseeking ] = useState(false);

	const onPlayPauseHandler = () => {
		setPlaying(!isPlaying);
	};

	const onVolumeChangeHandler = (e, newValue) => {
		setVolumeState({
			...volumeState,
			volume: parseFloat(newValue / 100),
			muted: newValue === 0 ? true : false
		});
	};

	const onMuteHandler = () => {
		setVolumeState({ ...volumeState, muted: !volumeState.muted });
	};

	const onProgressHandler = ({ played }) => {
		if (count.current > 3) {
			videoControlsRef.current.style.visibility = 'hidden';
			count.current = 0;
		}
		if (videoControlsRef.current.style.visibility === 'visible') {
			count.current += 1;
		}
		if (!seeking) {
			setPlayed(played);
		}
	};

	const onMouseMoveHandler = () => {
		videoControlsRef.current.style.visibility = 'visible';
		count.current = 0;
	};

	const onMouseLeaveHandler = () => {
		videoControlsRef.current.style.visibility = 'hidden';
		count.current = 0;
	};

	const hideTracks = () => {
		if (videoTrackRef.current) {
			for (let i = 0; i < videoRef.current.textTracks.length; i++) {
				videoRef.current.textTracks[i].mode = 'hidden';
				for (let k = 0; k < videoTrackRef.current.cues.length; k++) {
					videoTrackRef.current.removeCue(videoTrackRef.current.cues[k]);
				}
			}
		}
	};

	const onReadyHandler = () => {
		hideTracks();
		videoRef.current = innerContainerRef.current.querySelector('video');
		videoTrackRef.current = videoRef.current.addTextTrack('captions', 'English', 'en');
		videoTrackRef.current.mode = 'showing';
		for (let subtitle of subtitles) {
			const cue = new VTTCue(subtitle.startTime, subtitle.endTime, subtitle.text);
			cue.id = subtitle.id;
			videoTrackRef.current.addCue(cue);
		}
	};

	const onRewindHandler = () => {
		reactPlayerRef.current.seekTo(reactPlayerRef.current.getCurrentTime() - 10);
	};

	const onFastForwardHandler = () => {
		reactPlayerRef.current.seekTo(reactPlayerRef.current.getCurrentTime() + 10);
	};

	const currentTime = reactPlayerRef && reactPlayerRef.current ? reactPlayerRef.current.getCurrentTime() : '00:00';

	const duration = reactPlayerRef && reactPlayerRef.current ? reactPlayerRef.current.getDuration() : '00:00';

	const elapsedTime = format(currentTime);

	const totalDuration = format(duration);

	const onSeekHandler = (e, newValue) => {
		setPlayed(parseFloat(newValue / 100));
	};

	const onSeekMouseDownHandler = (e) => {
		setseeking(true);
	};

	const onSeekMouseUpHandler = (e, newValue) => {
		setseeking(false);
		reactPlayerRef.current.seekTo(newValue / 100, 'fraction');
	};

	return (
		<Container maxWidth="md">
			<div
				className={classes.innerContainer}
				ref={innerContainerRef}
				onMouseMove={onMouseMoveHandler}
				onMouseLeave={onMouseLeaveHandler}
			>
				<ReactPlayer
					ref={reactPlayerRef}
					width={'100%'}
					height={'100%'}
					// url="https://www.youtube.com/watch?v=WhWc3b3KhnY&feature=emb_logo"
					url="/videos/sample-video.mp4"
					controls={false}
					playing={isPlaying}
					loop={false}
					volume={volumeState.volume}
					muted={volumeState.muted}
					onProgress={onProgressHandler}
					onReady={onReadyHandler}
				/>

				<VideoControls
					ref={videoControlsRef}
					{...{
						onPlayPause: onPlayPauseHandler,
						played,
						isPlaying,
						volumeState,
						onVolumeChange: onVolumeChangeHandler,
						onMute: onMuteHandler,
						onRewind: onRewindHandler,
						onFastForward: onFastForwardHandler,
						elapsedTime,
						totalDuration,
						onSeek: onSeekHandler,
						onSeekMouseUp: onSeekMouseUpHandler,
						onSeekMouseDown: onSeekMouseDownHandler
					}}
				/>
			</div>
		</Container>
	);
};

export default VideoPlayer;
