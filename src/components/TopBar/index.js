import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';

const local = {
	TopBar: {
		title: 'Video Subtitling '
	}
};

const useStyles = makeStyles((theme) => ({
	appBar: {
		background: '#28282a'
	},
	title: {
		marginLeft: theme.spacing(1),
		color: theme.palette.secondary.main
	}
}));

const TopBar = () => {
	const classes = useStyles();
	const { title } = local.TopBar;

	return (
		<AppBar position="fixed" className={classes.appBar}>
			<Toolbar>
				<Typography variant="h6">{title}</Typography>
				<Typography variant="h6" className={classes.title}>
					{'App'}
				</Typography>
			</Toolbar>
		</AppBar>
	);
};

export default TopBar;
