import React, { createContext } from 'react';
import { useLocalStorageReducer } from '../hooks/useLocalStorageReducer';
import subtitleReducer from '../reducers/subtitle.reducer.js';

const defaultSubtitles = [
	{ id: 1, startTime: 0.05, endTime: 12, text: '[Test Cues]' },
	{ id: 2, startTime: 18.7, endTime: 21.5, text: 'What makes you different that’s your strength.' },
	{ id: 3, startTime: 22.8, endTime: 26.8, text: 'A positive attitude causes a chain reaction' }
];
export const SubtitlesContext = createContext();
export const DispatchContext = createContext();

export const SubtitlessProvider = (props) => {
	const [ subtitles, dispatch ] = useLocalStorageReducer('subtitles', defaultSubtitles, subtitleReducer);
	return (
		<SubtitlesContext.Provider value={subtitles}>
			<DispatchContext.Provider value={dispatch}>{props.children}</DispatchContext.Provider>
		</SubtitlesContext.Provider>
	);
};
