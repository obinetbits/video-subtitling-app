import { v4 as uuid } from 'uuid';

const reducer = (state, action) => {
	switch (action.type) {
		case 'ADD':
			return [
				...state,
				{
					id: uuid(),
					startTime: action.payload.startTime,
					endTime: action.payload.endTime,
					text: action.payload.text
				}
			];
		case 'REMOVE':
			return state.filter((subtitle) => subtitle.id !== action.payload.id);
		case 'EDIT':
			return state.map(
				(subtitle) =>
					subtitle.id === action.payload.id
						? {
								...subtitle,
								startTime: action.payload.startTime,
								endTime: action.payload.endTime,
								text: action.payload.text
							}
						: subtitle
			);
		default:
			return state;
	}
};
export default reducer;
